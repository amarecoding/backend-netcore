using System;

namespace webapi.Models
{
    public class exam_detail
    {
        public long id { get; set; }
        public long question_id { get; set; }
        public int number_answer { get; set; }
        public long exam_id { get; set; }
    }
}