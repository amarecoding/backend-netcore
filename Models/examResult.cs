using System.Collections.Generic;

namespace webapi.Models
{
    public class examResult
    {
        public exam exam { get; set; }
        public List<exam_detail> exam_details { get; set; }
    }
}