namespace webapi.Models
{
    public class question
    {
        public long id { get; set; }
        public string text_question { get; set; }
        public int number_answer_success { get; set; }
        public int lessonid { get; set; }
        public decimal value_answer { get; set; }
    }
}