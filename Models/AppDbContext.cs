using Microsoft.EntityFrameworkCore;

namespace webapi.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<student> students { get; set; }
        public DbSet<course> courses { get; set; }
        public DbSet<lesson> lessons { get; set; }
        public DbSet<question> questions { get; set; }
        public DbSet<answer> answers { get; set; }
        public DbSet<exam> exams { get; set; }
        public DbSet<exam_detail> exams_details { get; set; }
    }
}