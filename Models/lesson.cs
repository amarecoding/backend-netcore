namespace webapi.Models
{
    public class lesson
    {
        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public long courseid { get; set; }
    }
}