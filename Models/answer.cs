namespace webapi.Models
{
    public class answer
    {
        public long id { get; set; }
        public string text_answer { get; set; }
        public int number_answer { get; set; }
        public long questionid { get; set; }
    }
}