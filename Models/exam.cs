using System;
using System.Collections.Generic;

namespace webapi.Models
{
    public class exam
    {
        public long id { get; set; }
        public long studentid { get; set; }
        public bool passed { get; set; }
        public decimal result { get; set; }
        public DateTime date { get; set; }
    }
}