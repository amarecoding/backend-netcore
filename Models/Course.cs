namespace webapi.Models
{
    public class course
    {
        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
    }
}