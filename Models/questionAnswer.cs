using System.Collections.Generic;

namespace webapi.Models
{
    public class questionAnswer
    {
        public question question { get; set; }
        public List<answer> answers { get; set; }
    }
}