using System.Collections.Generic;
using Npgsql;
using webapi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace webapi.Services
{
    public class CourseService
    {
        private string _connection = "";
        public CourseService(string connection)
        {
            _connection = connection;
        }

        public course Post(AppDbContext _ctx, course course)
        {
            _ctx.courses.Add(course);
            _ctx.SaveChanges();
            return course;
        }

        public List<course> GetCourses(AppDbContext _ctx)
        {
            return _ctx.courses.OrderBy(o => o.id).ToList();
        }

        public course GetCourse(AppDbContext _ctx, long id)
        {
            return _ctx.courses.Find(id);
        }

        public course Put(AppDbContext _ctx, long id, course course)
        {
            if (id != course.id)
            {
                return null;
            }

            _ctx.Entry(course).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_ctx.courses.Find(id) == null)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return course;
        }

        public course Delete(AppDbContext _ctx, long id)
        {
            var course = _ctx.courses.Find(id);
            if (course == null)
            {
                return null;
            }

            _ctx.courses.Remove(course);
            _ctx.SaveChanges();

            return course;
        }

        public List<course> GetCoursesByStudent(int id)
        {
            List<course> courses = new List<course>();
            var conn = new NpgsqlConnection(_connection);
            conn.Open();
            using (var cmd = new NpgsqlCommand("SELECT * FROM courses_getlistbystudent(@p)", conn))
            {
                cmd.Parameters.AddWithValue("p", id);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            course course = new course();
                            course.id = (long)reader["id"];
                            course.name = reader["name"].ToString();
                            course.description = reader["description"].ToString();
                            courses.Add(course);
                        }
                    }

                    reader.Close();
                }
            }

            return courses;
        }
    }
}