using System.Collections.Generic;
using Npgsql;
using webapi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace webapi.Services
{
    public class ExamService
    {
        public List<exam> GetExams(AppDbContext _ctx)
        {
            return _ctx.exams.OrderBy(o => o.id).ToList();
        }

        public examResult GetExam(AppDbContext _ctx, long id)
        {
            examResult examResult = new examResult();
            examResult.exam = _ctx.exams.Find(id);
            examResult.exam_details = _ctx.exams_details.Where(x => x.exam_id == id).ToList();
            return examResult;
        }

        public examResult Post(AppDbContext _ctx, examResult examResult)
        {
            List<exam_detail> exam_Details = new List<exam_detail>();

            _ctx.exams.Add(examResult.exam);
            _ctx.SaveChanges();

            long exam_id = examResult.exam.id;

            examResult.exam_details.ForEach(f =>
            {
                f.exam_id = exam_id;
                exam_Details.Add(f);
            });

            _ctx.exams_details.AddRange(exam_Details);
            _ctx.SaveChanges();

            return examResult;
        }
    }
}