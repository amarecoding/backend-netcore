using System.Collections.Generic;
using Npgsql;
using webapi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace webapi.Services
{
    public class LessonsService
    {
        private string _connection = "";
        public LessonsService(string connection)
        {
            _connection = connection;
        }

        public lesson Post(AppDbContext _ctx, lesson lesson)
        {
            _ctx.lessons.Add(lesson);
            _ctx.SaveChanges();
            return lesson;
        }

        public List<lesson> GetLessons(AppDbContext _ctx)
        {
            return _ctx.lessons.OrderBy(o => o.id).ToList();
        }

        public lesson GetLesson(AppDbContext _ctx, long id)
        {
            return _ctx.lessons.Find(id);
        }

        public lesson Put(AppDbContext _ctx, long id, lesson lesson)
        {
            if (id != lesson.id)
            {
                return null;
            }

            _ctx.Entry(lesson).State = EntityState.Modified;

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_ctx.lessons.Find(id) == null)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return lesson;
        }

        public lesson Delete(AppDbContext _ctx, long id)
        {
            var lesson = _ctx.lessons.Find(id);
            if (lesson == null)
            {
                return null;
            }

            _ctx.lessons.Remove(lesson);
            _ctx.SaveChanges();

            return lesson;
        }

        public List<lesson> GetLessonsByCourse(AppDbContext _ctx, int id)
        {
            return _ctx.lessons.Where(x => x.courseid == id).ToList();
        }

        public List<lesson> GetLessonsByStudent(AppDbContext _ctx, int student_id)
        {
            List<lesson> lessons = new List<lesson>();
            var conn = new NpgsqlConnection(_connection);
            conn.Open();
            using (var cmd = new NpgsqlCommand("SELECT * FROM lessons_getlistbystudent(@p)", conn))
            {
                cmd.Parameters.AddWithValue("p", student_id);
                using (var reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            lesson lesson = new lesson();
                            lesson.id = (long)reader["id"];
                            lesson.name = reader["name"].ToString();
                            lesson.description = reader["description"].ToString();
                            lesson.courseid = (long)reader["courseid"];
                            lessons.Add(lesson);
                        }
                    }
                    reader.Close();
                }
            }

            return lessons;
        }

        public List<questionAnswer> GetDetailLesson(AppDbContext _ctx, int lesson_id)
        {
            List<questionAnswer> ListQuestionAnswers = new List<questionAnswer>();
            List<question> questions = _ctx.questions.Where(x => x.lessonid == lesson_id).OrderBy(o => o.id).ToList();

            if (questions.Count > 0)
            {
                questions.ForEach(q =>
                {
                    questionAnswer SetQuestionAnswers = new questionAnswer();
                    SetQuestionAnswers.question = q;
                    SetQuestionAnswers.answers = _ctx.answers.Where(x => x.questionid == q.id).ToList();
                    ListQuestionAnswers.Add(SetQuestionAnswers);
                });
            }

            return ListQuestionAnswers;
        }
    }
}