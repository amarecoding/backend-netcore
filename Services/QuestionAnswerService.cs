using System.Collections.Generic;
using webapi.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace webapi.Services
{
    public class QuestionAnswerService
    {
        public questionAnswer GetQuestionAnswer(AppDbContext _ctx, long id)
        {
            questionAnswer questionAnswer = new questionAnswer();
            questionAnswer.question = _ctx.questions.Find(id);
            questionAnswer.answers = _ctx.answers.Where(x => x.questionid == id).ToList();

            if (questionAnswer.question == null && questionAnswer.answers.Count == 0)
            {
                return null;
            }

            return questionAnswer;
        }

        public List<question> GetQuestionAnswers(AppDbContext _ctx)
        {
            return _ctx.questions.OrderBy(o => o.id).ToList();
        }

        public List<questionAnswer> Post(AppDbContext _ctx, List<questionAnswer> questionAnswers)
        {
            questionAnswers.ForEach(q =>
            {
                List<answer> answers = new List<answer>();

                /* SAVE  QUESTION*/
                _ctx.questions.Add(q.question);
                _ctx.SaveChanges();

                q.answers.ForEach(a =>
                {
                    a.questionid = q.question.id;
                    answers.Add(a);
                });

                _ctx.answers.AddRange(answers);
                _ctx.SaveChanges();

            });

            return questionAnswers;
        }

        public questionAnswer Put(AppDbContext _ctx, long id, questionAnswer questionAnswer)
        {
            if (id != questionAnswer.question.id)
            {
                return null;
            }

            question question = questionAnswer.question;
            List<answer> answers = questionAnswer.answers;

            _ctx.Entry(question).State = EntityState.Modified;

            answers.ForEach(a =>
            {
                _ctx.Entry(a).State = EntityState.Modified;
            });

            try
            {
                _ctx.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_ctx.questions.Find(questionAnswer.question.id) == null)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }

            return questionAnswer;
        }

        public question Delete(AppDbContext _ctx, long id)
        {
            var answers = _ctx.answers.Where(x => x.questionid == id).ToList();
            var question = _ctx.questions.Find(id);

            if (answers.Count > 0 && question != null)
            {
                _ctx.answers.RemoveRange(answers);
                _ctx.questions.Remove(question);
                _ctx.SaveChanges();

                return question;
            }
            else
            {
                return null;
            }
        }
    }
}