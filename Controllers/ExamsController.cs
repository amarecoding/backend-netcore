using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExamsController : ControllerBase
    {
        private readonly AppDbContext _ctx;

        public ExamsController(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        public ActionResult<List<exam>> GetExams()
        {
            List<exam> exams = new ExamService().GetExams(_ctx);
            return Ok(exams);
        }

        [HttpGet("{id}")]
        public ActionResult<examResult> GetExam(long id)
        {
            examResult exam = new ExamService().GetExam(_ctx, id);
            return Ok(exam);
        }

        [HttpPost]
        public ActionResult<object> PostExam(examResult examResult)
        {
            examResult ExamenResultPost = new ExamService().Post(_ctx, examResult);
            return Ok(new { success = "true", entitie = ExamenResultPost });
        }
    }
}