using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LessonsController : ControllerBase
    {
        private readonly AppDbContext _ctx;
        private readonly IConfiguration _configuration;

        public LessonsController(AppDbContext ctx, IConfiguration configuration)
        {
            _ctx = ctx;
            _configuration = configuration;
        }


        [HttpPost]
        public ActionResult<object> PostLesson(lesson lesson)
        {
            new LessonsService(GetConnection()).Post(_ctx, lesson);
            return Ok(new { success = "true", entitie = lesson });
        }


        [HttpGet]
        public ActionResult<List<lesson>> GetLessons()
        {
            List<lesson> lessons = new LessonsService(GetConnection()).GetLessons(_ctx);
            return Ok(lessons);
        }


        [HttpGet("{id}")]
        public ActionResult<List<lesson>> GetLesson(long id)
        {
            lesson lesson = new LessonsService(GetConnection()).GetLesson(_ctx, id);
            return Ok(lesson);
        }


        [HttpPut("{id}")]
        public ActionResult<object> PutLesson(long id, lesson lesson)
        {
            lesson putLesson = new LessonsService(GetConnection()).Put(_ctx, id, lesson);

            if (putLesson != null)
            {
                return Ok(new { success = "true", rows = 0, data = putLesson });
            }
            else
            {
                return NotFound();
            }
        }


        [HttpDelete("{id}")]
        public ActionResult<object> DeleteLesson(long id)
        {
            lesson lesson = new LessonsService(GetConnection()).Delete(_ctx, id);
            if (lesson != null)
            {
                return Ok(new { success = "true", rows = 0, data = lesson });
            }
            else
            {
                return NotFound();
            }
        }


        [HttpGet("course")]
        public ActionResult<List<lesson>> GetLessonsByCourse(int id)
        {
            List<lesson> lessons = new LessonsService(GetConnection()).GetLessonsByCourse(_ctx, id);
            return Ok(new { success = "true", rows = lessons.Count, data = lessons });
        }


        [HttpGet("student")]
        public ActionResult<List<lesson>> GetLessonsByStudent(int id)
        {
            List<lesson> lessons = new LessonsService(GetConnection()).GetLessonsByStudent(_ctx, id);
            return Ok(new { success = "true", rows = lessons.Count, data = lessons });
        }


        [HttpGet("detail")]
        public ActionResult<List<questionAnswer>> GetDetailLesson(int id)
        {
            List<questionAnswer> takeLesson = new LessonsService(GetConnection()).GetDetailLesson(_ctx, id);
            return Ok(new { success = "true", rows = takeLesson.Count, data = takeLesson });
        }

        private string GetConnection()
        {
            return _configuration.GetConnectionString("PostgresSQL");
        }
    }
}