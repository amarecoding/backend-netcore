using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QuestionAnswerController : ControllerBase
    {
        private readonly AppDbContext _ctx;

        public QuestionAnswerController(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        [HttpPost]
        public ActionResult<object> PostQuestionAnswer(List<questionAnswer> questionAnswers)
        {
            var questionAnswersPost = new QuestionAnswerService().Post(_ctx, questionAnswers);
            return Ok(new { success = "true", entitie = questionAnswersPost });
        }

        [HttpGet]
        public ActionResult<List<question>> GetQuestionAnswers()
        {
            List<question> questions = new QuestionAnswerService().GetQuestionAnswers(_ctx);
            return Ok(questions);
        }

        [HttpGet("{id}")]
        public ActionResult<questionAnswer> GetQuestionAnswer(long id)
        {
            questionAnswer questionAnswer = new QuestionAnswerService().GetQuestionAnswer(_ctx, id);
            if (questionAnswer == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(questionAnswer);
            }
        }

        [HttpPut("{id}")]
        public ActionResult<object> PutQuestionAnswer(long id, questionAnswer questionAnswer)
        {
            questionAnswer putQuestionAnswer = new QuestionAnswerService().Put(_ctx, id, questionAnswer);
            if (putQuestionAnswer != null)
            {
                return Ok(new { sucess = "true", rows = 0, data = putQuestionAnswer });
            }
            else
            {
                return NotFound();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<object> DeleteQuestionAnswer(long id)
        {
            var questionAnswers = new QuestionAnswerService().Delete(_ctx, id);
            if (questionAnswers != null)
            {
                return Ok(new { success = "true", rows = 0, data = questionAnswers });
            }
            else
            {
                return NotFound();
            }
        }
    }
}