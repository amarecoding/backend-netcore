using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using webapi.Models;
using webapi.Services;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CoursesController : ControllerBase
    {
        private readonly AppDbContext _ctx;
        private readonly IConfiguration _configuration;

        public CoursesController(AppDbContext ctx, IConfiguration configuration)
        {
            _ctx = ctx;
            _configuration = configuration;
        }

        [HttpPost]
        public ActionResult<object> PostCourse(course course)
        {
            course CoursePost = new CourseService(GetConnection()).Post(_ctx, course);
            return Ok(new { success = "true", entitie = CoursePost });
        }


        [HttpGet]
        public ActionResult<List<course>> GetCourses()
        {
            List<course> courses = new CourseService(GetConnection()).GetCourses(_ctx);
            return Ok(courses);
        }


        [HttpGet("{id}")]
        public ActionResult<course> GetCourse(long id)
        {
            course course = new CourseService(GetConnection()).GetCourse(_ctx, id);
            return Ok(course);
        }


        [HttpPut("{id}")]
        public ActionResult<object> PutCourse(long id, course course)
        {
            course putCurse = new CourseService(GetConnection()).Put(_ctx, id, course);

            if (putCurse != null)
            {
                return Ok(new { success = "true", rows = 0, data = putCurse });
            }
            else
            {
                return NotFound();
            }
        }


        [HttpDelete("{id}")]
        public ActionResult<object> DeleteCourse(long id)
        {
            course course = new CourseService(GetConnection()).Delete(_ctx, id);
            if (course != null)
            {
                return Ok(new { success = "true", rows = 0, data = course });
            }
            else
            {
                return NotFound();
            }
        }


        [HttpGet("student")]
        public ActionResult<object> GetCoursesByStudent(int id)
        {
            List<course> courses = new CourseService(GetConnection()).GetCoursesByStudent(id);
            return Ok(new { success = "true", rows = courses.Count, data = courses });
        }

        private string GetConnection()
        {
            return _configuration.GetConnectionString("PostgresSQL");
        }
    }
}