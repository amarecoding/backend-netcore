using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using webapi.Models;

namespace webapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly AppDbContext _ctx;

        public StudentsController(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        [HttpGet]
        public ActionResult<List<student>> Get()
        {
            return Ok(_ctx.students);
        }
    }
}