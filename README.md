# Examen Backend Developer

#### Requerimientos principales para ejecutar el proyecto
1.- Tener instalado la version 3.1.100 de netcore


#### Instalacion de Dependencias
Ejecutar en el root de proyecto "dotnet restore" , este comando es equivalente a npm install para poder ejecutar la instalacion
de librerias de terceros


#### Ejecutar el proyecto
Ejecutar en el root de proyecto "dotnet run" para levantar el servidor local, endpoint de ejemplo https://localhost:5001/api/courses


#### Lista de endpoints disponibles:

1.- Students
	
	* https://localhost:5001/api/students
	
2.- Courses
	
	* https://localhost:5001/api/courses	 GET
	* https://localhost:5001/api/courses/5   GET
	* https://localhost:5001/api/courses	 POST
	* https://localhost:5001/api/courses/5   PUT
	* https://localhost:5001/api/courses/5   DELETE
	* https://localhost:5001/api/courses/5   PUT
	* https://localhost:5001/api/courses/student?id=2 GET
	
3.- Lessons

	* https://localhost:5001/api/lessons	   			GET
	* https://localhost:5001/api/lessons/10    			GET
	* https://localhost:5001/api/lessons       			POST
	* https://localhost:5001/api/lessons/10    			PUT
	* https://localhost:5001/api/lessons/10    			DELETE
	* https://localhost:5001/api/lessons/course?id=1 	GET
	* https://localhost:5001/api/lessons/student?id=2   GET
	* https://localhost:5001/api/lessons/detail?id=21	GET
	
4.- Questions

	* https://localhost:5001/api/questionanswer/ 		GET
	* https://localhost:5001/api/questionanswer/1		GET
	* https://localhost:5001/api/questionanswer			POST
	* https://localhost:5001/api/questionanswer/1		PUT
	* https://localhost:5001/api/questionanswer/1		DELETE
	
5.- Exams
	
	* https://localhost:5001/api/exams/					GET
	* https://localhost:5001/api/exams/1				GET
	* https://localhost:5001/api/exams/					POST
	
	
	